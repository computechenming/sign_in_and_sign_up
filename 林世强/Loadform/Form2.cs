﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loadform
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        //注册新账号
        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            //获取文本信息
            var newUid = nName.Text;
            var newPwd = nPwd.Text;
            var rnewPwd = rnPwd.Text;

            var isUidNull = string.IsNullOrEmpty(newUid);
            var ispwdNull = string.IsNullOrEmpty(newPwd);
            var iscPwdNull = string.IsNullOrEmpty(rnewPwd);


            //判断是否注册成功
            if ((!isUidNull) && (!ispwdNull) && ispwdNull == iscPwdNull)
            {
                MessageBox.Show("恭喜你，注册成功！","注册成功",MessageBoxButtons.YesNo);
                Form1.User.Add(newUid, newPwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败，请重新注册！","注册失败", MessageBoxButtons.YesNo);
            }
        }

        //取消注册
        private void button2_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("取消成功！","取消注册", MessageBoxButtons.YesNo);
            this.Close();
        }
    }
}
