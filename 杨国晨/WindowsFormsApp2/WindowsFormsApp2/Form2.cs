﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void userPwd_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {



        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                userPwd.PasswordChar = new char();
            }
            else
            {
                userPwd.PasswordChar = '*';
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                userPwd.PasswordChar = new char();
            }
            else
            {
                userPwd.PasswordChar = '*';
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uId = userName.Text;
            var pwd = userPwd.Text;
            var cPwd = crypwd.Text;

            var isUidNull = string.IsNullOrEmpty(uId);
            var ispwdNull = string.IsNullOrEmpty(pwd);
            var iscPwdNull = string.IsNullOrEmpty(cPwd);

            if ((!isUidNull) && (!ispwdNull) && ispwdNull == iscPwdNull)
            {
                MessageBox.Show("恭喜你，注册成功！！");
                Form1.user.Add(uId, pwd);
                this.Close();
            }
            else
            {

                MessageBox.Show("很遗憾，注册失败，请重新注册！！");
            }
        }

    }
}