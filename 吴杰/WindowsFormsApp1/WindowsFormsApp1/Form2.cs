﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void register_Click(object sender, EventArgs e)
        {
            var uid = username1.Text;
            var pwd = userpwd1.Text;
            var rpwd = ruserpwd1.Text;

            var isUidNull =string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqual = pwd == rpwd;

           if(! isUidNull && ! isPwdNull && isEqual)
            {
                MessageBox.Show("恭喜你注册成功！");
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("不好意思注册失败！");
            }
        }
    }
}
