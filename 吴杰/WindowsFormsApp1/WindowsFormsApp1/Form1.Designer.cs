﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.UserName = new System.Windows.Forms.Label();
            this.UserPwd = new System.Windows.Forms.Label();
            this.uesrname = new System.Windows.Forms.ComboBox();
            this.uesrpwd = new System.Windows.Forms.ComboBox();
            this.Enter = new System.Windows.Forms.Button();
            this.Register = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UserName
            // 
            this.UserName.AutoSize = true;
            this.UserName.Location = new System.Drawing.Point(216, 62);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(53, 12);
            this.UserName.TabIndex = 0;
            this.UserName.Text = "用户名：";
            // 
            // UserPwd
            // 
            this.UserPwd.AutoSize = true;
            this.UserPwd.Location = new System.Drawing.Point(216, 107);
            this.UserPwd.Name = "UserPwd";
            this.UserPwd.Size = new System.Drawing.Size(41, 12);
            this.UserPwd.TabIndex = 1;
            this.UserPwd.Text = "密码：";
            // 
            // uesrname
            // 
            this.uesrname.FormattingEnabled = true;
            this.uesrname.Location = new System.Drawing.Point(338, 54);
            this.uesrname.Name = "uesrname";
            this.uesrname.Size = new System.Drawing.Size(121, 20);
            this.uesrname.TabIndex = 2;
            // 
            // uesrpwd
            // 
            this.uesrpwd.FormattingEnabled = true;
            this.uesrpwd.Location = new System.Drawing.Point(338, 104);
            this.uesrpwd.Name = "uesrpwd";
            this.uesrpwd.Size = new System.Drawing.Size(121, 20);
            this.uesrpwd.TabIndex = 2;
            // 
            // Enter
            // 
            this.Enter.Location = new System.Drawing.Point(218, 173);
            this.Enter.Name = "Enter";
            this.Enter.Size = new System.Drawing.Size(71, 26);
            this.Enter.TabIndex = 3;
            this.Enter.Text = "登入";
            this.Enter.UseVisualStyleBackColor = true;
            this.Enter.Click += new System.EventHandler(this.Enter_Click);
            // 
            // Register
            // 
            this.Register.Location = new System.Drawing.Point(366, 173);
            this.Register.Name = "Register";
            this.Register.Size = new System.Drawing.Size(71, 26);
            this.Register.TabIndex = 3;
            this.Register.Text = "注册";
            this.Register.UseVisualStyleBackColor = true;
            this.Register.Click += new System.EventHandler(this.Register_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Register);
            this.Controls.Add(this.Enter);
            this.Controls.Add(this.uesrpwd);
            this.Controls.Add(this.uesrname);
            this.Controls.Add(this.UserPwd);
            this.Controls.Add(this.UserName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UserName;
        private System.Windows.Forms.Label UserPwd;
        private System.Windows.Forms.ComboBox uesrname;
        private System.Windows.Forms.ComboBox uesrpwd;
        private System.Windows.Forms.Button Enter;
        private System.Windows.Forms.Button Register;
    }
}

