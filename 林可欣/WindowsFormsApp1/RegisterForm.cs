﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var againpwd = againpassWord.Text;

            var isUidNull = string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == againpwd;

            if (!isUidNull && !isPwdNull && isEqure)
            {
                MessageBox.Show("注册成功", "恭喜您！", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败","很遗憾！",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            }
        }

    }
}
